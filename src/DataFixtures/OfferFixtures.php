<?php

namespace App\DataFixtures;

use App\Entity\Offer;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class OfferFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function getDependencies()
    {
        return [
            PictureFixtures::class,
            CategoryFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $faker = Factory::create('fr_FR');

        $user = new User();

        $user->setEmail('admin@mail.com')
            ->setFirstname($faker->firstName)
            ->setLastName($faker->lastName)
            ->setRoles(['ROLE_ADMIN'])
            ->setIsVerified(true);

        $password = $this->encoder->encodePassword($user, 'demorasa');
        $user->setPassword($password);

        $this->manager->persist($user);

        for ($i = 0; $i < 5; $i++) {
            $offer = new Offer();

            $picture = $this->getReference("picture{$i}");

            $offer->setTitle($faker->words(3, true))
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setDescription($faker->text(300))
                ->setSlug($faker->slug(3))
                ->setIsAvailable($faker->randomElement([true, false]))
                ->addCategory($this->getReference("category" . mt_rand(1, 3)))
                ->addPicture($picture)
                ->setUser($user);

            $this->manager->persist($offer);

            $picture->setOffer($offer);
        }
        $this->manager->flush();
    }
}
