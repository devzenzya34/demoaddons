<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

/* Service de gestion des upload de pictures*/
class FileUploader
{
    private SluggerInterface $slugger;

    private $uploadsDirectory;

    public function __construct(SluggerInterface $slugger, string $uploadsDirectory)
    {
        $this->slugger = $slugger;
        $this->uploadsDirectory = $uploadsDirectory;
    }


    /**
     * @param UploadedFile $file
     * @return array
     */
    public function upload(UploadedFile $file): array
    {
        $fileName = $this->generateUniqFileName($file);
        try {
            $file->move($this->uploadsDirectory, $fileName);
        } catch (FileException $fileException) {
            throw $fileException;
        }
        return [
            'fileName' => $fileName,
            'filePath' => $this->uploadsDirectory . $fileName
        ];
    }


    /**
     * @param UploadedFile $file
     * @return string
     */
    private function generateUniqFileName(UploadedFile $file): string
    {
        $originalFileName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $originalFileNameSlugged = $this->slugger->slug(strtolower($originalFileName));

        $randoID = uniqid();

        return "{$originalFileNameSlugged}-{$randoID}.{$file->guessExtension()}";
    }
}