<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\Security\Core\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Http\Event\DeauthenticatedEvent;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class AuthenticatorSubscriber implements EventSubscriberInterface
{
    private LoggerInterface $securityLogger;

    private RequestStack $requestStack;

    public function __construct(LoggerInterface $securityLogger, RequestStack $requestStack)
    {
        $this->securityLogger = $securityLogger;
        $this->requestStack = $requestStack;
    }

    /**
     * @return array<string>
     */
    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onSecurityAuthenticationFailure',
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onSecurityAuthenticationSuccess',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
            'security.logout_on_change' => 'onSecurityLogoutChange',
            SecurityEvents::SWITCH_USER => 'onSecuritySwitchUser'
        ];
    }

    public function onSecurityAuthenticationFailure(AuthenticationFailureEvent $event): void
    {
       dd($event);
    }

    public function onSecurityAuthenticationSuccess(AuthenticationSuccessEvent $event): void
    {
        /*//dd($event);
        [
            'route_name' => $route_name,
            'user_IP' => $user_IP
        ] = $this->getRouteNameAndUserIP();

        if(empty($event->getAuthenticationToken()->getRoleNames())) {
            $this->securityLogger->info(" Un user anonyme avec l'adresse IP '{$user_IP}' est apparu sur la route '{$route_name}':-) ! ");
        } else {
            /** @var TokenInterface $securityToken
            $securityToken = $event->getAuthenticationToken();

            $userEmail = $this->getUserEmail($securityToken);
            $this->securityLogger->info("Un user anonyme ayant l'adrese IP '{$user_IP}' a évolué en entité User avec l'email '{$userEmail} :-) !'");
        }*/
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event): void
    {

    }

    public function onSecurityLogoutChange(DeauthenticatedEvent $event)
    {

    }

    public function onSecuritySwitchUSer(SwitchUserEvent $event)
    {

    }

    /**
     * Méthode pour recupérer l' URL et l'adresse IP du USer
     * @return array{user_IP: string|null, route_name: mixed}
     */
    private function getRouteNameAndUserIP(): array
    {
        $request = $this->requestStack->getCurrentRequest();

        if(!$request) {
            return [
                'user_IP' => 'Unknown',
                'route_name' => 'Unknown'
            ];
        }

        return [
            'user_IP' => $request->getClientIp(),
            'route_name' => $request->attributes->get('_route')
        ];
    }

    private function getUserEmail() : string
    {
        $user = $securityToken->getUser();

        return $user->getEmail();
    }


}
