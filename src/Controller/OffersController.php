<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Form\OfferType;
use App\Repository\OfferRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OffersController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(OfferRepository $OfferRepository): Response
    {
        $offers = $OfferRepository->findBy([], ['createdAt' => 'DESC']);

        return $this->render('offers/index.html.twig', compact('offers'));
    }
    
    /**
     * @Route("/offers/create", name="app_offers_create", methods={"GET", "POST"})
     */
    public function create(Request $request, EntityManagerInterface $em, UserRepository $userRepo): Response
    {
        $pin = new Offer;

        $form = $this->createForm(OfferType::class, $pin);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pin->setUser($this->getUser());
            $em->persist($pin);
            $em->flush();

            $this->addFlash('success', 'Offer successfully created!');

            return $this->redirectToRoute('app_home');
        }

        return $this->render('offers/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/offers/{id<[0-9]+>}", name="app_offers_show", methods={"GET"})
     */
    public function show(Offer $offer): Response
    {
        return $this->render('offers/show.html.twig', compact('offer'));
    }
    
    /**
     * @Route("/offers/{id<[0-9]+>}/edit", name="app_offers_edit", methods={"GET", "PUT"})
     */
    public function edit(Request $request, Offer $offer, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(OfferType::class, $offer, [
            'method' => 'PUT'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Offer successfully updated!');

            return $this->redirectToRoute('app_home');
        }

        return $this->render('offers/edit.html.twig', [
            'offer' => $offer,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/offers/{id<[0-9]+>}", name="app_offers_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Offer $pin, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('PIN_MANAGE', $pin);

        if ($this->isCsrfTokenValid('pin_deletion_' . $pin->getId(), $request->request->get('csrf_token'))) {
            $em->remove($pin);
            $em->flush();

            $this->addFlash('info', 'Offer successfully deleted!');
        }

        return $this->redirectToRoute('app_home');
    }
}
